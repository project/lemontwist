<?php

/*
 * Regions
 */

function lemontwist_regions() {
  return array(
    'right' => t('right sidebar'),
    'content' => t('content'),
    'footer' => t('footer'),
  );
}
